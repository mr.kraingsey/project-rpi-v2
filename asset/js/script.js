$(document).ready(function(){
    $('.phone-menu-btn').click(function(){
        $('.phone-menu').slideToggle(500)
    })
    $(document).ready(function(){
        var items = [
            {name: "ជំនាញមេកានិក", img:"slide5.jpg"},
            {name: "អគ្គិសនី", img: "slide4.jpg"},
            {name: "អេឡិចត្រូនិច", img: "slide4.jpg"}
        ]
        getSlide();
        function getSlide(){
            var txt ="";
            items.forEach((e)=>{
                txt +=`
                <div class="slide">
                    <img src="asset/img/img-slide/${e['img']}" alt="">
                    <div class="content">
                        <h2>${e['name']}</h1>
                        <hr style="border: 2px dashed white">
                        <h1>WELCOME TO <br>REGINAL POLYTECHNIC INSTITUTE TECHO SEN SVAY RIENG</h1>
                        <hr style="border: 2px dashed white">
                        <h3>TVET ជំនាញពិតជីវិតប្រសើរ</h3>
                        <button type="button">ចុះឈ្មោះចូលរៀន</button>
                    </div>
                </div>
                `
            })
            console.log(txt)
            $(".slide-container").find('.box').append(txt);
        }
        var slide = $('.slide');
        var numSlide = slide.length;
        console.log(slide)
        var indSlide = 0;
        slide.hide();
        slide.eq(0).show();
        function nextSlide(){
            slide.eq(indSlide).hide();
            indSlide++;
            if(indSlide == numSlide){
                indSlide =0;
            }
            slide.eq(indSlide).show();
        }
        var mySlide = setInterval(function(){
            nextSlide();
        },6000);
        $('.slide-container').mouseover(function(){
            clearInterval(mySlide);
        })
        $('.slide-container').mouseleave(function(){
            mySlide = setInterval(function(){
                nextSlide();
            },6000);
        })
        $('.btnNext').click(function(){
            nextSlide();        
        })
        $('.btnBack').click(function(){
            slide.eq(indSlide).hide();
            indSlide--;
            if(indSlide < 0){
                indSlide = numSlide - 1;
            }
            slide.eq(indSlide).show()
        })
        
    })
})